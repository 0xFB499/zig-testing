const std = @import("std");

fn nextInt(reader: anytype, comptime T: type) anyerror!T {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var arena = std.heap.ArenaAllocator.init(gpa.allocator());
    defer arena.deinit();

    var buff = std.ArrayList(u8).init(arena.allocator());
    while (reader.readByte()) |b| {
        switch (b) {
            ' ', '\n' => break,
            else => try buff.append(b),
        }
    } else |err| {
        if (err != error.EndOfStream) return err;
    }
    const buff_slice = try buff.toOwnedSlice();
    return try std.fmt.parseInt(T, buff_slice, 10);
}

test "next integer" {
    const stdin = std.io.getStdIn().reader();
    const stdout = std.io.getStdOut().writer();
    const c = nextInt(stdin, u8);
    if (c) |v| {
        try std.testing.expect(v == 12);
    } else |err| {
        try stdout.print("{}\n", .{@TypeOf(err)});
    }
}
