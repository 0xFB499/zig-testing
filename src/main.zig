const std = @import("std");
const mem = std.mem;
const os = std.os;
const stdout = std.io.getStdOut().writer();

pub fn main() !void {
    var tty = try std.fs.openFileAbsolute("/dev/tty", .{ .mode = .read_write });
    var win_size = mem.zeroes(os.linux.winsize);
    const err = os.linux.ioctl(tty.handle, os.linux.T.IOCGWINSZ, @intFromPtr(&win_size));
    if (os.errno(err) != .SUCCESS) {
        return os.unexpectedErrno(@enumFromInt(err));
    }
    try stdout.print("ws_row: {}\nws_cols: {}\nws_xpixel: {}\nws_ypixel: {}\n", .{ win_size.ws_row, win_size.ws_col, win_size.ws_xpixel, win_size.ws_ypixel });
}
